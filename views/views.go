package views

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"regexp"
	"time"

	"github.com/go-chi/render"
	"github.com/go-redis/redis/v8"
	"github.com/gorilla/csrf"
	"gitlab.com/zadiv/surpercharged-todo/graph/auth"
	"gitlab.com/zadiv/surpercharged-todo/graph/db"
	"gitlab.com/zadiv/surpercharged-todo/graph/errors"
	"gitlab.com/zadiv/surpercharged-todo/graph/model"
)

type LoginRequest struct {
	Username string `json:"username"`
	Password string `json:"password"`
}

type SignUpRequest struct {
	Username  string   `json:"username"`
	Email     string   `json:"email"`
	Password1 string   `json:"password1"`
	Password2 string   `json:"password2"`
	Bio       *string  `json:"bio"`
	Roles     []string `json:"roles"`
}

type IView interface {
	Login(w http.ResponseWriter, r *http.Request)
	Logout(w http.ResponseWriter, r *http.Request)
	SignUp(w http.ResponseWriter, r *http.Request)
	Refresh(w http.ResponseWriter, r *http.Request)
}

type View struct {
	UM           *db.UserManager
	AuthService  auth.IAuth
	TokenService auth.IToken
	RedisClient  *redis.Client
}

func NewView(u *db.UserManager, tokenService auth.IToken, authService auth.IAuth) *View {
	return &View{UM: u, AuthService: authService, TokenService: tokenService}
}

func (v *View) Login(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("X-CSRF-Token", csrf.Token(r))
	_, cancel := context.WithTimeout(r.Context(), 450*time.Millisecond)
	defer cancel()
	r.ParseForm()
	username := r.Form.Get("username")
	password := r.Form.Get("password")
	if len(username) == 0 {
		http.Error(w, "username is a required field", http.StatusBadRequest)
		return
	}
	if len(password) == 0 {
		http.Error(w, "password is a required field", http.StatusBadRequest)
		return
	}

	user, err := v.UM.Get(context.TODO(), map[string]interface{}{"username": username})
	if err != nil {
		http.Error(w, "please provide valid login details", http.StatusBadRequest)
		return
	}

	if v.UM.CheckPassword(password, user.Password) {
		http.Error(w, "please provide valid login details", http.StatusBadRequest)
		return
	}
	ts, err := v.TokenService.CreateToken(user)
	if err != nil {
		http.Error(w, err.Error(), http.StatusUnprocessableEntity)
		return
	}
	err = v.AuthService.CreateAuth(context.TODO(), user, ts)
	if err != nil {
		http.Error(w, err.Error(), http.StatusUnprocessableEntity)
		return
	}
	token := map[string]interface{}{
		"access_token":  ts.AccessToken,
		"refresh_token": ts.RefreshToken,
	}
	w.Header().Set("Content-Type", "application/json")
	if err := json.NewEncoder(w).Encode(token); err != nil {
		render.Render(w, r, errors.ErrRender(err))
	}

}

func (v *View) Logout(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("X-CSRF-Token", csrf.Token(r))
	metaData, _ := v.TokenService.ExtractTokenMetadata(r)
	if metaData != nil {
		deleteErr := v.AuthService.DeleteTokens(context.TODO(), metaData)
		if deleteErr != nil {
			render.Render(w, r, errors.ErrInvalidRequest(deleteErr))
		}
	}
	render.JSON(w, r, "done")
}

func (v *View) SignUp(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("X-CSRF-Token", csrf.Token(r))
	regExp := regexp.MustCompile(`\[(.*?)\]`)
	lctx, cancel := context.WithTimeout(r.Context(), 450*time.Millisecond)
	defer cancel()
	r.ParseForm()
	username := r.Form.Get("username")
	email := r.Form.Get("email")
	password1 := r.Form.Get("password1")
	password2 := r.Form.Get("password2")
	bio := r.Form.Get("bio")
	role_ids := r.Form.Get("role_ids")
	if password1 != password2 {
		render.Render(w, r, errors.ErrInvalidRequest(fmt.Errorf("password missmatch")))
	}
	// TODO: ensure that those field are not empty we will use validate here
	_, err := v.UM.Create(lctx, model.User{
		Username: username,
		Email:    email,
		Password: password2,
		Bio:      bio,
		RoleIDs:  regExp.FindAllString(role_ids, -1),
	})
	if err != nil {
		render.Render(w, r, errors.ErrRender(err))
	}
	render.JSON(w, r, "success")
}

func (v *View) Refresh(w http.ResponseWriter, r *http.Request) {
	panic("not implemented")
}
