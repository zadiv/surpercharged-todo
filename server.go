package main

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"os"
	"os/signal"
	"sync"
	"time"

	"github.com/99designs/gqlgen/graphql/handler"
	"github.com/99designs/gqlgen/graphql/handler/extension"
	"github.com/99designs/gqlgen/graphql/handler/transport"
	"github.com/99designs/gqlgen/graphql/playground"
	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
	"github.com/go-redis/redis/v8"
	"github.com/gorilla/csrf"
	"github.com/gorilla/websocket"
	"github.com/joho/godotenv"
	"github.com/rs/cors"
	"gitlab.com/zadiv/surpercharged-todo/graph"
	"gitlab.com/zadiv/surpercharged-todo/graph/auth"
	"gitlab.com/zadiv/surpercharged-todo/graph/db"
	"gitlab.com/zadiv/surpercharged-todo/graph/generated"
	"gitlab.com/zadiv/surpercharged-todo/graph/model"
	"gitlab.com/zadiv/surpercharged-todo/views"
	r "gopkg.in/rethinkdb/rethinkdb-go.v6"
)

const defaultPort = "8080"

func NewRedis(host, port, password string) *redis.Client {
	return redis.NewClient(&redis.Options{
		Addr:     host + ":" + port,
		Password: password,
		DB:       0,
	})
}

func subscribe(session *r.Session, quit <-chan bool) {
	changeObserver := make(chan r.ChangeResponse, 1)
	cursor, _ := r.Table("todos").Changes().Run(session)

	go func() {
		var change r.ChangeResponse
		for cursor.Next(&change) {
			changeObserver <- change
		}
	}()

	for {
		select {
		case <-quit:
			cursor.Close()
			return
		case change := <-changeObserver:
			fmt.Printf("%#v\n", change.NewValue)
		}
	}
}

func main() {
	router := chi.NewRouter()
	// A good base middleware stack
	router.Use(middleware.RequestID)
	router.Use(middleware.RealIP)
	router.Use(middleware.Logger)
	router.Use(middleware.Recoverer)

	// CORS
	router.Use(cors.New(cors.Options{
		AllowedOrigins:   []string{"http://localhost:3000", "http://localhost:8082/"},
		AllowCredentials: true,
		Debug:            true,
	}).Handler)

	if err := godotenv.Load(".env"); err != nil {
		log.Fatal(err)
	}
	port := os.Getenv("PORT")
	if port == "" {
		port = defaultPort
	}

	secretKey := os.Getenv("SECRET_KEY")
	redisHost := os.Getenv("REDIS_HOST")
	redisPort := os.Getenv("REDIS_PORT")
	// redisPassword := os.Getenv("REDIS_PASSWORD")

	redisClient := NewRedis(redisHost, redisPort, "")
	tokenService := auth.NewTokenService()
	authService := auth.NewAuthService(redisClient)

	var (
		once    sync.Once
		session *r.Session
	)

	once.Do(func() {
		session = db.Init()
	})

	um := db.NewUserManager(session, "users")
	rm := db.NewRoleManager(session, "roles")
	tm := db.NewTodoManager(session, "todos")

	srv := handler.NewDefaultServer(generated.NewExecutableSchema(generated.Config{Resolvers: &graph.Resolver{
		UM: um, RM: rm, TM: tm, RedisClient: redisClient, TodoChannels: make(chan *model.Todo, 1), Session: session,
	}}))

	srv.AddTransport(transport.POST{})
	srv.AddTransport(transport.Websocket{
		KeepAlivePingInterval: 10 * time.Second,
		Upgrader: websocket.Upgrader{
			CheckOrigin: func(r *http.Request) bool {
				// Check against your desired domains here
				//  return r.Host == "example.org"
				log.Println(r)
				return true
			},
			ReadBufferSize:  1024,
			WriteBufferSize: 1024,
		},
	})

	srv.Use(extension.Introspection{})
	csrfMiddleware := csrf.Protect([]byte(secretKey),
		csrf.TrustedOrigins([]string{"http://localhost:3000", "http://localhost:8082"}), csrf.Path("/"))
	view := views.NewView(um, tokenService, authService)

	server := &http.Server{
		Addr:    fmt.Sprintf(":%s", port),
		Handler: router,
	}

	router.Group(func(r chi.Router) {
		r.Route("/auth", func(r chi.Router) {
			r.Use(csrfMiddleware)
			r.Post("/login", view.Login)
			r.Post("/signup", view.SignUp)
			r.Post("/logout", view.Login)
			r.Post("/refresh", view.Refresh)
		})
		r.Group(func(r chi.Router) {
			// r.Use(auth.AuthMiddleware)
			r.Handle("/", playground.Handler("GraphQL playground", "/query"))
			r.Handle("/query", srv)
		})
	})

	log.Printf("connect to http://localhost:%s/ for GraphQL playground", port)
	go func() {
		if err := server.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			log.Fatalf("ListenAndServe: %s\n", err)
		}
	}()
	//Wait for interrupt signal to gracefully shutdown the server with a timeout of 10 seconds
	quit := make(chan os.Signal)
	signal.Notify(quit, os.Interrupt)
	<-quit
	log.Println("Shutdown Server ...")

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	if err := server.Shutdown(ctx); err != nil {
		log.Fatal("Server Shutdown:", err)
	}
	log.Println("Server exiting")

}
