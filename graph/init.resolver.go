package graph

import (
	"log"
	"os"

	"github.com/joho/godotenv"
)

var (
	dbName        string
	roleTableName string
	userTableName string
)

func init() {
	if err := godotenv.Load(".env"); err != nil {
		log.Fatal(err)
	}
	dbName = os.Getenv("DB_NAME")
	roleTableName = os.Getenv("ROLE_TABLE_NAME")
	userTableName = os.Getenv("USER_TABLE_NAME")
}
