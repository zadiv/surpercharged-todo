package graph

// This file will be automatically regenerated based on the schema, any resolver implementations
// will be copied through when generating and any unknown code will be moved to the end.

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"time"

	"gitlab.com/zadiv/surpercharged-todo/graph/generated"
	"gitlab.com/zadiv/surpercharged-todo/graph/model"
	rd "gopkg.in/rethinkdb/rethinkdb-go.v6"
)

func (r *mutationResolver) CreateTodo(ctx context.Context, input model.NewTodo) (*model.Todo, error) {
	now := time.Now()
	todo := model.Todo{
		Text:        input.Text,
		Description: input.Description,
		CreatedAt:   &now,
		UserIDs:     input.UserIDs,
		UserID:      input.UserID,
		Start:       &input.Start,
		End:         &input.End,
	}
	res, err := r.TM.Create(ctx, todo)
	if err != nil {
		return nil, err
	}
	// if err := r.RedisClient.Publish(ctx, "todo-added", res).Err(); err != nil {
	// 	return nil, err
	// }

	return res, nil
}

func (r *mutationResolver) UpdateTodo(ctx context.Context, input model.UpdateTodo) (*model.Todo, error) {
	todo := model.Todo{
		ID:          input.ID,
		Text:        input.Text,
		Done:        input.Done,
		UserID:      input.UserID,
		UserIDs:     input.UserIDs,
		Description: input.Description,
		End:         &input.End,
		Start:       &input.Start,
	}
	res, err := r.TM.Update(ctx, todo)
	if err != nil {
		return nil, err
	}
	return res, nil
}

func (r *mutationResolver) DeleteTodo(ctx context.Context, input map[string]interface{}) (bool, error) {
	if err := r.TM.Delete(ctx, input); err != nil {
		return false, err
	}
	return true, nil
}

func (r *mutationResolver) CreateUser(ctx context.Context, input model.NewUser) (*model.User, error) {
	now := time.Now()
	user := model.User{
		Username:  input.Username,
		Password:  input.Password,
		Email:     input.Email,
		Bio:       input.Bio,
		RoleIDs:   input.Role,
		CreatedAt: &now,
	}
	res, err := r.UM.Create(ctx, user)
	if err != nil {
		return nil, err
	}
	return res, nil
}

func (r *mutationResolver) UpdateUser(ctx context.Context, input model.UpdateUser) (*model.User, error) {
	var tmpPwd string
	if input.Password1 != input.Password2 {
		return nil, errors.New("password missmatch")
	}
	tmpPwd = input.Password2
	now := time.Now()

	user := model.User{
		ID:          input.ID,
		Username:    input.Username,
		Password:    tmpPwd,
		OldPassword: input.OldPassword,
		Email:       input.Email,
		Bio:         input.Bio,
		RoleIDs:     input.Role,
		UpdatedAt:   &now,
	}
	res, err := r.UM.Update(ctx, user)
	if err != nil {
		return nil, err
	}
	return res, nil
}

func (r *mutationResolver) DeleteUser(ctx context.Context, input map[string]interface{}) (bool, error) {
	if err := r.UM.Delete(ctx, input); err != nil {
		return false, err
	}
	return true, nil
}

func (r *mutationResolver) CreateRole(ctx context.Context, input model.NewRole) (*model.Role, error) {
	now := time.Now()
	role := model.Role{
		Name:        input.Name,
		Description: *input.Description,
		CreatedAt:   &now,
	}
	res, err := r.RM.Create(ctx, role)
	if err != nil {
		return nil, err
	}

	return res, nil
}

func (r *mutationResolver) UpdateRole(ctx context.Context, input model.UpdateRole) (*model.Role, error) {
	res, err := r.RM.Update(ctx, input)
	if err != nil {
		return nil, err
	}
	return res, nil
}

func (r *mutationResolver) DeleteRole(ctx context.Context, input map[string]interface{}) (bool, error) {
	if err := r.RM.Delete(ctx, input); err != nil {
		return false, err
	}
	return true, nil
}

func (r *queryResolver) Todos(ctx context.Context, filter map[string]interface{}, limit int, page int) ([]*model.Todo, error) {
	res, err := r.TM.All(ctx, filter, limit, page)
	if err != nil {
		return nil, err
	}
	return res, nil
}

func (r *queryResolver) Todo(ctx context.Context, filter map[string]interface{}) (*model.Todo, error) {
	res, err := r.TM.Get(ctx, filter)
	if err != nil {
		return nil, err
	}
	return res, nil
}

func (r *queryResolver) Users(ctx context.Context, filter map[string]interface{}, limit int, page int) ([]*model.User, error) {
	res, err := r.UM.All(ctx, filter, limit, page)
	if err != nil {
		return nil, err
	}
	return res, nil
}

func (r *queryResolver) User(ctx context.Context, filter map[string]interface{}) (*model.User, error) {
	res, err := r.UM.Get(ctx, filter)
	if err != nil {
		return nil, err
	}
	return res, nil
}

func (r *queryResolver) Roles(ctx context.Context, filter map[string]interface{}, limit int, page int) ([]*model.Role, error) {
	res, err := r.RM.All(ctx, filter, limit, page)
	if err != nil {
		return nil, err
	}
	return res, nil
}

func (r *queryResolver) Role(ctx context.Context, filter map[string]interface{}) (*model.Role, error) {
	res, err := r.RM.Get(ctx, filter)
	if err != nil {
		return nil, err
	}
	return res, nil
}

func (r *queryResolver) Search(ctx context.Context, query string, limit int, page int) ([]model.SearchResult, error) {
	var (
		res         []model.SearchResult
		searchError []error
	)

	users, uerr := r.UM.Search(ctx, query)
	if uerr != nil {
		searchError = append(searchError, uerr)
	}
	todos, terr := r.TM.Search(ctx, query)
	if terr != nil {
		searchError = append(searchError, terr)
	}
	roles, rerr := r.RM.Search(ctx, query)
	if rerr != nil {
		searchError = append(searchError, rerr)
	}
	if len(searchError) > 0 {
		return nil, fmt.Errorf("%s", searchError)
	}
	if len(users) > 0 {
		for _, u := range users {
			res = append(res, u)
		}
	}
	if len(todos) > 0 {
		for _, t := range todos {
			res = append(res, t)
		}
	}
	if len(roles) > 0 {
		for _, r := range roles {
			res = append(res, r)
		}
	}
	return res, nil
}

func (r *subscriptionResolver) TodoAdded(ctx context.Context) (<-chan *model.Todo, error) {
	changeObserver := make(chan rd.ChangeResponse, 1)
	cursor, err := rd.Table("todos").Changes(rd.ChangesOpts{IncludeInitial: true}).Run(r.Session)
	if err != nil {
		return nil, err
	}

	go func() {
		var change rd.ChangeResponse
		for cursor.Next(&change) {
			changeObserver <- change
		}
	}()
	for {
		select {
		case <-ctx.Done():
			cursor.Close()
			return nil, nil
		case <-changeObserver:
			change := <-changeObserver
			var todo model.Todo
			data, err := json.Marshal(change.NewValue)
			if err != nil {
				return nil, err
			}
			if err := json.Unmarshal([]byte(data), &todo); err != nil {
				return nil, err
			}
			r.TodoChannels <- &todo
			return r.TodoChannels, nil

		}
	}
}

// Mutation returns generated.MutationResolver implementation.
func (r *Resolver) Mutation() generated.MutationResolver { return &mutationResolver{r} }

// Query returns generated.QueryResolver implementation.
func (r *Resolver) Query() generated.QueryResolver { return &queryResolver{r} }

// Subscription returns generated.SubscriptionResolver implementation.
func (r *Resolver) Subscription() generated.SubscriptionResolver { return &subscriptionResolver{r} }

type mutationResolver struct{ *Resolver }
type queryResolver struct{ *Resolver }
type subscriptionResolver struct{ *Resolver }
