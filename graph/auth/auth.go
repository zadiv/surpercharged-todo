package auth

import (
	"errors"
	"log"
	"net/http"
	"os"
	"strings"
	"time"

	"github.com/gofrs/uuid"
	"github.com/golang-jwt/jwt"
	"github.com/joho/godotenv"
	"gitlab.com/zadiv/surpercharged-todo/graph/db"
	"gitlab.com/zadiv/surpercharged-todo/graph/model"
)

var (
	accessKey  string
	refreshKey string
)

func init() {
	if err := godotenv.Load(".env"); err != nil {
		log.Fatal(err)
	}
	accessKey = os.Getenv("ACCESS_KEY")
	refreshKey = os.Getenv("REFRESH_KEY")

}

type tokenService struct{}
type contextKey struct {
	name string
}

func NewTokenService() *tokenService {
	return &tokenService{}
}

type IToken interface {
	CreateToken(user *model.User) (*TokenDetails, error)
	ExtractTokenMetadata(r *http.Request) (*AccessDetails, error)
}

var userCtxKey = &contextKey{"user"}

type Auth struct {
	UM *db.UserManager
}

func validateAndGetUserID(cookie *http.Cookie) (string, error) {
	panic("not implemented")
}

func (ts *tokenService) CreateToken(user *model.User) (*TokenDetails, error) {

	td := &TokenDetails{}
	td.AtExpires = time.Now().Add(time.Minute * 40).Unix() // expires after 10 min max
	uid, err := uuid.NewV4()
	if err != nil {
		return nil, err
	}
	td.TokenUUID = uid.String()

	td.RtExpires = time.Now().Add(time.Hour * 24 * 3).Unix() // expires after 72H
	uid, err = uuid.NewV4()
	if err != nil {
		return nil, err
	}
	td.RefreshUUID = uid.String()

	atClaims := jwt.MapClaims{}
	atClaims["user_id"] = user.ID
	atClaims["username"] = user.Username
	atClaims["access_uuid"] = td.TokenUUID
	atClaims["exp"] = td.AtExpires

	at := jwt.NewWithClaims(jwt.SigningMethodHS256, atClaims)
	td.AccessToken, err = at.SignedString([]byte(accessKey))
	if err != nil {
		return nil, err
	}
	// Creating Refresh Token
	td.RtExpires = time.Now().Add(time.Hour * 24 * 3).Unix()
	td.RefreshUUID = td.TokenUUID + "++" + user.ID

	rtClaims := jwt.MapClaims{}
	rtClaims["refresh_uuid"] = td.RefreshUUID
	rtClaims["user_id"] = user.ID
	rtClaims["username"] = user.Username
	rtClaims["exp"] = td.RtExpires

	rt := jwt.NewWithClaims(jwt.SigningMethodHS256, rtClaims)
	td.RefreshToken, err = rt.SignedString([]byte(refreshKey))

	if err != nil {
		return nil, err
	}

	return td, nil
}

func TokenValid(r *http.Request) error {
	token, err := verifyToken(r)
	if err != nil {
		log.Println("Verify Token Error:", err)
		return err
	}
	if _, ok := token.Claims.(jwt.Claims); !ok && !token.Valid {
		log.Println("Verify Token Claims Error:", err)
		return err
	}
	return nil
}

func (ts *tokenService) ExtractTokenMetadata(r *http.Request) (*AccessDetails, error) {
	token, err := verifyToken(r)
	if err != nil {
		log.Println("Verify Token Error", err)
		return nil, err
	}
	acc, err := extract(token)
	if err != nil {
		log.Println("Extract Error: ", err)
		return nil, err
	}
	return acc, nil
}

func ExtractTokenMetaData(r *http.Request) (*AccessDetails, error) {
	token, err := verifyToken(r)
	if err != nil {
		return nil, err
	}
	acc, err := extract(token)
	if err != nil {
		return nil, err
	}
	return acc, nil
}

func verifyToken(r *http.Request) (*jwt.Token, error) {
	tokenString := extractToken(r)
	token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, errors.New("missing or malformed jwt")
		}
		return []byte(accessKey), nil
	})
	if err != nil {

		return nil, err
	}
	return token, nil
}

//extractToken get the token from the request body
func extractToken(r *http.Request) string {
	bearToken := r.Header.Get("Authorization")
	strArr := strings.Split(bearToken, " ")
	if len(strArr) == 2 {
		return strArr[1]
	}
	return ""
}

func extract(token *jwt.Token) (*AccessDetails, error) {
	claims, ok := token.Claims.(jwt.MapClaims)
	if ok && token.Valid {
		accessUuid, ok := claims["access_uuid"].(string)
		userId, userOk := claims["user_id"].(string)
		username, uOk := claims["username"].(string)
		if !ok || !userOk || !uOk {
			return nil, errors.New("unauthorized")
		} else {
			return &AccessDetails{
				TokenUUID: accessUuid,
				UserID:    userId,
				Username:  username,
			}, nil
		}
	}
	return nil, errors.New("something went wrong")
}

// AuthMiddleware decode the sahre session cookie and packs the session into context
func AuthMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		err := TokenValid(r)
		if err != nil {
			http.Error(w, "unauthorized", http.StatusUnauthorized)
			return
		}
		next.ServeHTTP(w, r)
	})
}
