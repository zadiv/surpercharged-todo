package auth

import (
	"context"
	"errors"
	"fmt"
	"log"
	"time"

	"github.com/go-redis/redis/v8"
	"gitlab.com/zadiv/surpercharged-todo/graph/model"
)

type IAuth interface {
	CreateAuth(context.Context, *model.User, *TokenDetails) error
	FetchAuth(context.Context, string) (string, error)
	DeleteRefresh(context.Context, string) error
	DeleteTokens(context.Context, *AccessDetails) error
}

type authService struct {
	client *redis.Client
}

func NewAuthService(client *redis.Client) *authService {
	return &authService{client: client}
}

type AccessDetails struct {
	TokenUUID string
	UserID    string
	Username  string
}

type TokenDetails struct {
	AccessToken  string
	RefreshToken string
	TokenUUID    string
	RefreshUUID  string
	//AtExpires AccessTokenExpires time until access token become invalid
	AtExpires int64
	//RtExpires RefreshTokenExpires time until refresh token become invalid
	RtExpires int64
}

func (as *authService) CreateAuth(ctx context.Context, user *model.User, td *TokenDetails) error {
	lctx, cancel := context.WithTimeout(ctx, 350*time.Millisecond)
	defer cancel()
	at := time.Unix(td.AtExpires, 0) // Converting Unix to UTC(to time object)
	rt := time.Unix(td.RtExpires, 0)
	now := time.Now()

	atCreated, err := as.client.Set(lctx, td.TokenUUID, user, at.Sub(now)).Result()
	if err != nil {
		return err
	}

	rtCreated, err := as.client.Set(lctx, td.TokenUUID, user, rt.Sub(now)).Result()
	if err != nil {
		return err
	}
	if atCreated == "0" || rtCreated == "0" {
		return errors.New("no record inserted")
	}
	return nil
}

// FetchAuth check back the metadata saved
func (as *authService) FetchAuth(ctx context.Context, tokenUUID string) (string, error) {
	lctx, cancel := context.WithTimeout(ctx, 800*time.Millisecond)
	defer cancel()
	userID, err := as.client.Get(lctx, tokenUUID).Result()
	if err != nil {
		return "", err
	}
	return userID, nil
}

func (as *authService) DeleteRefresh(ctx context.Context, tokenUUID string) error {
	lctx, cancel := context.WithTimeout(ctx, 350*time.Millisecond)
	defer cancel()
	deleted, err := as.client.Del(lctx, tokenUUID).Result()
	if err != nil || deleted == 0 {
		return err
	}
	return nil
}

func (as *authService) DeleteTokens(ctx context.Context, ad *AccessDetails) error {
	lctx, cancel := context.WithTimeout(ctx, 350*time.Millisecond)
	defer cancel()
	refreshUUID := fmt.Sprintf("%s++%s", ad.TokenUUID, ad.UserID)
	// delete access token
	deletedAt, err := as.client.Del(lctx, ad.TokenUUID).Result()
	if err != nil {
		return err
	}
	deletedRt, err := as.client.Del(lctx, refreshUUID).Result()
	if err != nil {
		return err
	}
	if deletedAt != 1 || deletedRt != 1 {
		log.Println(err)
		return errors.New("somenthing went wrong")
	}
	return nil
}
