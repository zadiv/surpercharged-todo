package db

import (
	"context"
	"errors"
	"log"
	"strings"
	"time"

	"gitlab.com/zadiv/surpercharged-todo/graph/model"
	"gitlab.com/zadiv/surpercharged-todo/helpers/text"
	r "gopkg.in/rethinkdb/rethinkdb-go.v6"
)

type ITodo interface {
	Create(ctx context.Context, args model.Todo) (*model.Todo, error)
	Update(ctx context.Context, args model.Todo) (*model.Todo, error)
	Delete(ctx context.Context, filter map[string]interface{}) error
	Get(ctx context.Context, filter map[string]interface{}) (*model.Todo, error)
	All(ctx context.Context, filter map[string]interface{}, limit int, page int) ([]*model.Todo, error)
	Search(ctx context.Context, query string) ([]*model.Todo, error)
}

type TodoManager struct {
	Session *r.Session
	tblName string
}

func NewTodoManager(session *r.Session, tblName string) *TodoManager {
	return &TodoManager{Session: session, tblName: tblName}
}

func mergeUsers(todo r.Term) r.Term {
	return r.Table("users").GetAll(r.Args(todo.Field("user_ids"))).CoerceTo("array").
		Merge(func(user r.Term) interface{} {
			return map[string]interface{}{
				"roles": r.Table("roles").GetAll(r.Args(user.Field("role_ids"))).CoerceTo("array"),
			}
		})
}

func mergeUser(todo r.Term) r.Term {
	return r.Table("users").Get(todo.Field("user_id")).
		Merge(func(user r.Term) interface{} {
			return map[string]interface{}{
				"roles": r.Table("roles").GetAll(r.Args(user.Field("role_ids"))).CoerceTo("array"),
			}
		})
}

func (tm *TodoManager) Search(ctx context.Context, query string) ([]*model.Todo, error) {
	_, cancel := context.WithTimeout(ctx, 450*time.Millisecond)
	defer cancel()
	var todos []*model.Todo
	res, err := r.Table(tm.tblName).Filter(func(row r.Term) interface{} {
		return r.Expr([]string{"text", "description"}).Contains(func(key r.Term) interface{} {
			return row.Field(key).CoerceTo("string").Match("(?i)" + query)
		})
	}).Merge(func(todo r.Term) interface{} {
		return map[string]interface{}{
			"users": mergeUsers(todo),
			"user":  mergeUser(todo),
		}
	}).Run(tm.Session)
	if err != nil {
		return nil, err
	}
	err = res.All(&todos)
	if err != nil {
		return nil, err
	}
	return todos, nil
}

func (tm *TodoManager) Create(ctx context.Context, args model.Todo) (*model.Todo, error) {
	_, cancel := context.WithTimeout(ctx, 350*time.Millisecond)
	defer cancel()
	tmpStr := strings.Split(args.Text, " ")
	slug := text.Slugify(strings.Join(tmpStr[:2], " ")) // this will be too long maybe?
	args.Slug = &slug
	res, err := r.Table(tm.tblName).Insert(args).RunWrite(tm.Session)
	if err != nil {
		return nil, err
	}
	var (
		todoRes  *r.Cursor
		fetchErr error
	)
	if len(args.UserIDs) == 0 {
		todoRes, fetchErr = r.Table(tm.tblName).Get(res.GeneratedKeys[0]).
			Merge(func(todo r.Term) interface{} {
				return map[string]interface{}{
					"users": nil,
					"user":  mergeUser(todo),
				}
			}).
			Run(tm.Session)
		if fetchErr != nil {
			return nil, fetchErr
		}
	} else {
		todoRes, fetchErr = r.Table(tm.tblName).Get(res.GeneratedKeys[0]).
			Merge(func(todo r.Term) interface{} {
				return map[string]interface{}{
					"users": mergeUsers(todo),
					"user":  mergeUser(todo),
				}
			}).
			Run(tm.Session)
		if fetchErr != nil {
			return nil, fetchErr
		}
	}

	var todo model.Todo
	err = todoRes.One(&todo)
	if err != nil {
		return nil, err
	}
	return &todo, nil
}

func (tm *TodoManager) Update(ctx context.Context, args model.Todo) (*model.Todo, error) {
	_, cancel := context.WithTimeout(ctx, 350*time.Millisecond)
	defer cancel()
	tmpStr := strings.Split(args.Text, " ")
	slug := text.Slugify(strings.Join(tmpStr[:2], " "))
	args.Slug = &slug
	var (
		res *r.Cursor
		err error
	)
	if len(args.UserIDs) == 0 {
		res, err = r.Table(tm.tblName).Get(args.ID).Update(args).
			Merge(func(todo r.Term) interface{} {
				return map[string]interface{}{
					"users": nil,
					"user":  mergeUser(todo),
				}
			}).
			Run(tm.Session)
	} else {
		res, err = r.Table(tm.tblName).Get(args.ID).Update(args).
			Merge(func(todo r.Term) interface{} {
				return map[string]interface{}{
					"users": mergeUsers(todo),
					"user":  mergeUser(todo),
				}
			}).
			Run(tm.Session)
	}
	var todo model.Todo
	if err != nil {
		return nil, err
	}
	err = res.One(&todo)
	if err != nil {
		return nil, err
	}
	return &todo, nil
}

func (tm *TodoManager) Delete(ctx context.Context, filter map[string]interface{}) error {
	_, cancel := context.WithTimeout(ctx, 350*time.Millisecond)
	defer cancel()
	if len(filter) == 0 {
		return errors.New("you need to setup a filter like {'id': 'something-'}")
	}
	_, err := r.Table(tm.tblName).Filter(filter).Delete().RunWrite(tm.Session)
	if err != nil {
		return err
	}
	return nil
}

func (tm *TodoManager) Get(ctx context.Context, filter map[string]interface{}) (*model.Todo, error) {
	_, cancel := context.WithTimeout(ctx, 350*time.Millisecond)
	defer cancel()
	if len(filter) == 0 {
		return nil, errors.New("you need to setup a filter first")
	}
	res, err := r.Table(tm.tblName).Filter(filter).Merge(func(todo r.Term) interface{} {
		return map[string]interface{}{
			"users": mergeUsers(todo),
			"user":  mergeUser(todo),
		}
	}).Run(tm.Session)
	if err != nil {
		return nil, err
	}
	var todo model.Todo
	err = res.One(&todo)
	log.Printf("%+v", todo)
	if err != nil {
		return nil, err
	}
	return &todo, nil
}

func (tm *TodoManager) All(ctx context.Context, filter map[string]interface{}, limit int, page int) ([]*model.Todo, error) {
	_, cancel := context.WithTimeout(ctx, 650*time.Millisecond)
	defer cancel()

	res, err := r.Table(tm.tblName).Skip(page).Limit(limit).Filter(filter).Merge(func(todo r.Term) interface{} {
		return map[string]interface{}{
			"users": mergeUsers(todo),
			"user":  mergeUser(todo),
		}
	}).Run(tm.Session)
	if err != nil {
		return nil, err
	}
	var todos []*model.Todo
	err = res.All(&todos)
	if err != nil {
		return nil, err
	}
	return todos, nil
}
