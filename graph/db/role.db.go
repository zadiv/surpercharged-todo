package db

import (
	"context"
	"time"

	"gitlab.com/zadiv/surpercharged-todo/graph/model"
	"gitlab.com/zadiv/surpercharged-todo/helpers/text"
	r "gopkg.in/rethinkdb/rethinkdb-go.v6"
)

type IRole interface {
	Create(ctx context.Context, args model.Role) (*model.Role, error)
	Update(ctx context.Context, args model.UpdateRole) (*model.Role, error)
	Delete(ctx context.Context, filter map[string]interface{}) error
	Get(ctx context.Context, filter map[string]interface{}) (*model.Role, error)
	All(ctx context.Context, filter map[string]interface{}, limit int, page int) ([]*model.Role, error)
	Search(ctx context.Context, query string) ([]*model.Role, error)
}

type RoleManager struct {
	Session *r.Session
	tblName string
}

func NewRoleManager(session *r.Session, tblName string) *RoleManager {
	return &RoleManager{Session: session, tblName: tblName}
}

func (rm *RoleManager) Search(ctx context.Context, query string) ([]*model.Role, error) {
	_, cancel := context.WithTimeout(ctx, 450*time.Millisecond)
	defer cancel()
	var roles []*model.Role
	res, err := r.Table(rm.tblName).Filter(func(row r.Term) interface{} {
		return r.Expr([]string{"description", "name"}).Contains(func(key r.Term) interface{} {
			return row.Field(key).CoerceTo("string").Match("(?i)" + query)
		})
	}).Run(rm.Session)
	if err != nil {
		return nil, err
	}
	err = res.All(&roles)
	if err != nil {
		return nil, err
	}
	return roles, nil
}

func (rm *RoleManager) All(ctx context.Context, filter map[string]interface{}, limit int, page int) ([]*model.Role, error) {
	_, cancel := context.WithTimeout(ctx, 650*time.Millisecond)
	defer cancel()
	var data []*model.Role
	res, err := r.Table(rm.tblName).Skip(page).Limit(limit).Filter(filter).Run(rm.Session)
	if err != nil {
		return nil, err
	}
	err = res.All(&data)
	if err != nil {
		return nil, err
	}
	return data, nil
}
func (rm *RoleManager) Get(ctx context.Context, filter map[string]interface{}) (*model.Role, error) {
	_, cancel := context.WithTimeout(ctx, 450*time.Millisecond)
	defer cancel()
	res, err := r.Table(rm.tblName).Filter(filter).Run(rm.Session)
	if err != nil {
		return nil, err
	}
	var role model.Role
	err = res.One(&role)
	if err != nil {
		return nil, err
	}
	defer res.Close()
	return &role, nil
}

func (rm *RoleManager) Create(ctx context.Context, args model.Role) (*model.Role, error) {
	_, cancel := context.WithTimeout(ctx, 350*time.Millisecond)
	defer cancel()
	tmpStr := text.Slugify(args.Name)
	args.Slug = &tmpStr
	res, err := r.Table(rm.tblName).Insert(args).RunWrite(rm.Session)
	if err != nil {
		return nil, err
	}
	args.ID = res.GeneratedKeys[0]
	return &args, nil
}

func (rm *RoleManager) Update(ctx context.Context, args model.UpdateRole) (*model.Role, error) {
	_, cancel := context.WithTimeout(ctx, 350*time.Millisecond)
	defer cancel()
	now := time.Now()
	tmpStr := ""
	if args.Description != nil {
		tmpStr = *args.Description
	}
	update := map[string]interface{}{
		"name":        args.Name,
		"description": tmpStr,
		"updated_at":  &now,
		"slug":        text.Slugify(args.Name),
	}
	_, err := r.Table(rm.tblName).Get(args.ID).Update(update).RunWrite(rm.Session)
	if err != nil {
		return nil, err
	}
	var role model.Role
	res, err := r.Table(rm.tblName).Get(args.ID).Run(rm.Session)
	if err != nil {
		return nil, err
	}
	err = res.One(&role)
	if err != nil {
		return nil, err
	}
	defer res.Close()

	return &role, nil
}
func (rm *RoleManager) Delete(ctx context.Context, filter map[string]interface{}) error {
	_, cancel := context.WithTimeout(ctx, 350*time.Millisecond)
	defer cancel()
	_, err := r.Table(rm.tblName).Filter(filter).Delete().RunWrite(rm.Session)
	if err != nil {
		return err
	}
	return nil
}
