package db

import (
	"context"
	"errors"
	"time"

	"gitlab.com/zadiv/surpercharged-todo/graph/model"
	"gitlab.com/zadiv/surpercharged-todo/helpers/text"
	"golang.org/x/crypto/bcrypt"
	r "gopkg.in/rethinkdb/rethinkdb-go.v6"
)

type IUser interface {
	Create(ctx context.Context, args model.User) (*model.User, error)
	Update(ctx context.Context, args model.User) (*model.User, error)
	Delete(ctx context.Context, filter map[string]interface{}) error
	Get(ctx context.Context, filter map[string]interface{}) (*model.User, error)
	All(ctx context.Context, filter map[string]interface{}, limit int, page int) ([]*model.User, error)
	Search(ctx context.Context, query string) ([]*model.User, error)
}

type UserManager struct {
	Session *r.Session
	tblName string
}

func NewUserManager(session *r.Session, tblName string) *UserManager {
	return &UserManager{Session: session, tblName: tblName}
}

// hashPassword hash the user password before save it
func hashPassword(password string) (string, error) {
	bytes, err := bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
	return string(bytes), err
}

// CheckPassword compare the store and the incoming password the match a specific user account
func (um *UserManager) CheckPassword(password, hash string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(hash), []byte(password))
	return err == nil
}

func mergeRole(user r.Term) r.Term {
	return r.Table("roles").GetAll(r.Args(user.Field("role_ids"))).CoerceTo("array")
}

func (um *UserManager) Search(ctx context.Context, query string) ([]*model.User, error) {
	_, cancel := context.WithTimeout(ctx, 450*time.Millisecond)
	defer cancel()
	var users []*model.User
	res, err := r.Table(um.tblName).Filter(func(row r.Term) interface{} {
		return r.Expr([]string{"bio", "username"}).Contains(func(key r.Term) interface{} {
			return row.Field(key).CoerceTo("string").Match("(?i)" + query)
		})
	}).Merge(func(user r.Term) interface{} {
		return map[string]interface{}{
			"roles": mergeRole(user),
		}
	}).Run(um.Session)
	if err != nil {
		return nil, err
	}
	err = res.All(&users)
	if err != nil {
		return nil, err
	}
	return users, nil
}
func (um *UserManager) All(ctx context.Context, filter map[string]interface{}, limit int, page int) ([]*model.User, error) {
	_, cancel := context.WithTimeout(ctx, 650*time.Millisecond)
	defer cancel()
	var (
		users []*model.User
		res   *r.Cursor
		err   error
	)
	if len(filter) == 0 {
		res, err = r.Table(um.tblName).Skip(page).Limit(limit).Merge(func(user r.Term) interface{} {
			return map[string]interface{}{
				"roles": mergeRole(user),
			}
		}).Run(um.Session)
		if err != nil {
			return nil, err
		}

	} else {
		res, err = r.Table(um.tblName).Skip(page).Limit(limit).Filter(filter).
			Merge(func(p r.Term) interface{} {
				return map[string]interface{}{
					"roles": mergeRole(p),
				}
			}).Run(um.Session)
		if err != nil {
			return nil, err
		}
	}

	err = res.All(&users)
	if err != nil {
		return nil, err
	}
	defer res.Close()
	return users, nil
}
func (um *UserManager) Get(ctx context.Context, filter map[string]interface{}) (*model.User, error) {
	_, cancel := context.WithTimeout(ctx, 350*time.Millisecond)
	defer cancel()
	res, err := r.Table(um.tblName).Filter(filter).Merge(func(p r.Term) interface{} {
		return map[string]interface{}{
			"roles": mergeRole(p),
		}
	}).Run(um.Session)
	if err != nil {
		return nil, err
	}
	var user model.User
	if err != nil {
		return nil, err
	}
	err = res.One(&user)
	if err != nil {
		return nil, err
	}
	defer res.Close()
	return &user, nil
}

func (um *UserManager) Delete(ctx context.Context, filter map[string]interface{}) error {
	_, cancel := context.WithTimeout(ctx, 350*time.Millisecond)
	defer cancel()
	_, err := r.Table(um.tblName).Filter(filter).Delete().RunWrite(um.Session)
	if err != nil {
		return err
	}
	return nil
}

func (um *UserManager) Create(ctx context.Context, args model.User) (*model.User, error) {
	_, cancel := context.WithTimeout(ctx, 350*time.Millisecond)
	defer cancel()
	pwd, err := hashPassword(args.Password)
	if err != nil {
		return nil, err
	}
	args.Password = pwd
	args.Slug = text.Slugify(args.Username)
	res, err := r.Table(um.tblName).Insert(args).RunWrite(um.Session)
	if err != nil {
		return nil, err
	}
	args.ID = res.GeneratedKeys[0]
	return &args, nil
}

func (um *UserManager) Update(ctx context.Context, args model.User) (*model.User, error) {
	_, cancel := context.WithTimeout(ctx, 350*time.Millisecond)
	defer cancel()
	var user model.User
	pwd, err := hashPassword(args.Password)
	if err != nil {
		return nil, err
	}

	cursor, err := r.Table(um.tblName).Get(args.ID).Run(um.Session)
	if err != nil {
		return nil, err
	}
	err = cursor.One(&user) // it cannot decode role from string as it is now

	defer cursor.Close()
	if err != nil {
		return nil, err
	}

	if !um.CheckPassword(args.OldPassword, user.Password) {
		return nil, errors.New("invalid user account")
	} else {
		args.Slug = text.Slugify(args.Username)
		args.Password = pwd
		_, err := r.Table(um.tblName).Get(args.ID).Update(args).RunWrite(um.Session)
		if err != nil {
			return nil, err
		}
		finalRes, err := r.Table(um.tblName).Get(args.ID).
			Merge(func(p r.Term) interface{} {
				return map[string]interface{}{
					"roles": mergeRole(p),
				}
			}).Run(um.Session)
		if err != nil {
			return nil, err
		}
		err = finalRes.One(&user)
		defer finalRes.Close()
		if err != nil {
			return nil, err
		}

		return &user, nil
	}

}
