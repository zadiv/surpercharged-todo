//go:generate go run github.com/99designs/gqlgen generate
package graph

import (
	"github.com/go-redis/redis/v8"
	"gitlab.com/zadiv/surpercharged-todo/graph/db"
	"gitlab.com/zadiv/surpercharged-todo/graph/model"
	r "gopkg.in/rethinkdb/rethinkdb-go.v6"
)

// This file will not be regenerated automatically.
//
// It serves as dependency injection for your app, add any dependencies you require here.

type Resolver struct {
	UM           *db.UserManager
	RM           *db.RoleManager
	TM           *db.TodoManager
	RedisClient  *redis.Client
	TodoChannels chan *model.Todo
	Session      *r.Session
}
