package model

import (
	"encoding/json"
	"time"
)

type User struct {
	ID          string     `json:"id" rethinkdb:"id,omitempty" redis:"id,omitempty"`
	Username    string     `json:"username" rethinkdb:"username" redis:"username"`
	Slug        string     `json:"slug"`
	Email       string     `json:"email" rethinkdb:"email,omitempty" redis:"email"`
	Password    string     `json:"password"  rethinkdb:"password" redis:"-"`
	OldPassword string     `json:"-`
	Bio         string     `json:"bio"`
	Roles       []*Role    `json:"roles" rethinkdb:"roles"`
	RoleIDs     []string   `json:"role_ids" rethinkdb:"role_ids"`
	LastLogin   *time.Time `json:"lastLogin"`
	CreatedAt   *time.Time `json:"createdAt" rethinkdb:"created_at"`
	UpdatedAt   *time.Time `json:"updatedAt" rethinkdb:"updated_at"`
	DeletedAt   *time.Time `json:"deletedAt" rethinkdb:"deleted_at"`
}

func (User) IsBaseModel()    {}
func (User) IsSearchResult() {}

func (u *User) String() string {
	return u.Username
}

func (u *User) UnmarshalBinary(data []byte) error {
	// convert data to yours, let's assume its json data
	return json.Unmarshal(data, u)
}

func (u *User) MarshalBinary() ([]byte, error) {
	return json.Marshal(u)
}
