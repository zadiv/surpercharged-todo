package model

import (
	"encoding/json"
	"time"
)

type Role struct {
	ID          string     `json:"id" rethinkdb:"id,omitempty" redis:"id,omitempty"`
	Name        string     `json:"name" rethinkdb:"name" redis:"name"`
	Slug        *string    `json:"slug" rethinkdb:"slug,omitempty" redis:"slug,omitempty"`
	Description string     `json:"description" rethinkdb:"description,omitempty" redis:"description,omitempty"`
	CreatedAt   *time.Time `json:"createdAt" rethinkdb:"created_at"`
	UpdatedAt   *time.Time `json:"updatedAt" rethinkdb:"updated_at"`
	DeletedAt   *time.Time `json:"deletedAt" rethinkdb:"deleted_at"`
}

func (Role) IsBaseModel()    {}
func (Role) IsSearchResult() {}

func (r *Role) String() string {
	return r.Name
}

func (r *Role) UnmarshalBinary(data []byte) error {
	// convert data to yours, let's assume its json data
	return json.Unmarshal(data, r)
}

func (r *Role) MarshalBinary() ([]byte, error) {
	return json.Marshal(r)
}
