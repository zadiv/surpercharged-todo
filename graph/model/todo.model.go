package model

import (
	"encoding/json"
	"time"
)

type Todo struct {
	ID          string     `json:"id" rethinkdb:"id,omitempty" redis:"id,omitempty"`
	Text        string     `json:"text" rethinkdb:"text" redis:"text"`
	Done        bool       `json:"done" rethinkdb:"done" redis:"done"`
	User        *User      `json:"user" rethinkdb:"user"`
	UserID      string     `json:"user_id" rethinkdb:"user_id"`
	Users       []*User    `json:"users" rethinkdb:"users"`
	UserIDs     []string   `json:"user_ids" rethinkdb:"user_ids"`
	Description string     `json:"description" rethinkdb:"description,omitempty" redis:"description,omitempty"`
	Slug        *string    `json:"slug" rethinkdb:"slug,omitempty" redis:"slug,omitempty"`
	End         *time.Time `json:"end" rethinkdb:"end"`
	Start       *time.Time `json:"start" rethinkdb:"start"`
	CreatedAt   *time.Time `json:"createdAt" rethinkdb:"created_at"`
	UpdatedAt   *time.Time `json:"updatedAt" rethinkdb:"updated_at"`
	DeletedAt   *time.Time `json:"deletedAt" rethinkdb:"deleted_at"`
}

func (Todo) IsBaseModel()    {}
func (Todo) IsSearchResult() {}

func (t *Todo) UnmarshalBinary(data []byte) error {
	// convert data to yours, let's assume its json data
	return json.Unmarshal(data, t)
}

func (t *Todo) MarshalBinary() ([]byte, error) {
	return json.Marshal(t)
}
